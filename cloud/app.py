from lib.utils import determine_inventory_availability_for_widgets, determine_inventory_availability_for_later, get_parts
from flask import Flask, jsonify, request
import os
import requests
from iotery_python_server_sdk import Iotery
from lib.supply_chain_url import supply_chain_url
from lib.make_order import make_order

app = Flask(__name__)
PORT = os.getenv("PORT")

if PORT == None:
    PORT = 5000

# Replace these with your own Iotery teams uuids
iotery = Iotery(os.getenv("API_KEY"))
vehicle_1_device_uuid = "3a1af49c-ec8b-11e9-b548-d283610663ec"
coordinator_device_uuid = "6f6642f4-ec8c-11e9-b548-d283610663ec"
dispatch_vehicle_command_uuid = "314723ad-ec90-11e9-b548-d283610663ec"


# Coordinator beacon route for Webhook
@app.route("/teams/<teamID>/coordinator", methods=["POST"])
def receive_coordinator_data(teamID):

    # Get the body from the Iotery Webhook reported from the embedded domain
    body = request.json
    webhook_device_uuid = body["metadata"]["deviceUuid"]

    if webhook_device_uuid == coordinator_device_uuid:

        # Get up to date information from the supply chain system
        info = requests.get(supply_chain_url + "/teams/" +
                            teamID + "/info").json()
        widget_demand = info["demand"]
        vehicle_1_dispatched = body["in"]["packets"][0]["data"]["vehicle_1_dispatched"]

        # Determine if an order is ready for delivery or if there will be
        can_fulfill_now, number_to_fulfill_now = determine_inventory_availability_for_widgets(
            info["inventory"])

        can_fulfill_later, number_to_fulfill_later = determine_inventory_availability_for_later(
            info["inventory"])

        # Don't have enough widgets to fulfill an on-demand order?  Then we need to make an order for parts!
        if number_to_fulfill_now < widget_demand or (can_fulfill_now == False and can_fulfill_later == False):
            make_order(teamID, widget_demand)

        # If there is demand, and the vehicle is available to be dispached, send command
        if widget_demand > 0 \
                and vehicle_1_dispatched == 0 \
                and can_fulfill_now:
            # if the coordinator receives a demand request from supply chain, and a vehicle has not already been dispatched to address it, then command a vehicle to go!
            res = iotery.createDeviceCommandInstance(deviceUuid=coordinator_device_uuid, data={
                "commandTypeUuid": dispatch_vehicle_command_uuid})

    # reply to Iotery that we got everything ok
    return jsonify({"status": "ok"})

# Vehicle Route for Webhook
@app.route("/teams/<teamID>/vehicles", methods=["POST"])
def process_vehicle_data(teamID):
    body = request.json
    webhook_device_uuid = body["metadata"]["deviceUuid"]

    # Make sure that it is the device we want
    if webhook_device_uuid == vehicle_1_device_uuid:

        webhook_data_vehicle_id = body["in"]["packets"][0]["data"]["vehicle_id"]

        # Get information on demand
        info = requests.get(supply_chain_url + "/teams/" +
                            teamID + "/info").json()
        widget_demand = info["demand"]

        can_fulfill_now, number_to_fulfill_now = determine_inventory_availability_for_widgets(
            info["inventory"])

        # If we can fulfill, then do it
        if can_fulfill_now:
            res = requests.post(supply_chain_url +
                                "/teams/" + teamID + "/fulfill", json={
                                    "unitsFulfilled": widget_demand,
                                    "vehicleId": webhook_data_vehicle_id
                                })
            response = res.json()
            if "message" in response:
                # Catch any supply chain service error messages and print them
                print(response["message"])
            else:
                print("Fulfilled " + str(info["demand"]) + " widgets")

    # Tell Iotery everything is OK
    return jsonify({"status": "ok"})


if __name__ == "__main__":
    app.run(debug=True, port=PORT, host="0.0.0.0")
